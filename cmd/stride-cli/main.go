package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/google/uuid"
	"github.com/spf13/cobra"

	"bitbucket.org/atlassian/go-stride/pkg/stride"
)

func main() {
	cmd := &cobra.Command{
		Use:   "stride [message]",
		Short: "Send a message to a Stride conversation",
		Args:  cobra.MinimumNArgs(1),
		RunE:  cmdMessage,
	}
	cmd.PersistentFlags().String("clientid", os.Getenv("STRIDE_API_CLIENT_ID"), "Stride API client ID (env STRIDE_API_CLIENT_ID)")
	cmd.PersistentFlags().String("secret", os.Getenv("STRIDE_API_SECRET"), "Stride API secret (env STRIDE_API_SECRET)")
	cmd.PersistentFlags().String("token", os.Getenv("STRIDE_ROOM_TOKEN"), "Stride room token (env STRIDE_ROOM_TOKEN)")
	cmd.PersistentFlags().String("cloudid", os.Getenv("STRIDE_CLOUD_ID"), "Stride Cloud ID (env STRIDE_CLOUD_ID)")
	cmd.PersistentFlags().String("conversation", "", "Stride conversation name or ID")
	cmd.Flags().Bool("lookup", false, "Lookup a conversation, do not send a message")

	ApplicationCardCmd := &cobra.Command{
		Use:   "card [text] [title]",
		Short: "Send an ApplicationCard to a Stride Conversation",
		Args:  cobra.MinimumNArgs(2),
		RunE:  cmdApplicationCard,
	}
	ApplicationCardCmd.Flags().String("description", "", "Description text")
	ApplicationCardCmd.Flags().String("previewurl", "", "Image URL for preview")

	cmd.AddCommand(ApplicationCardCmd)

	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getClient(cmd *cobra.Command) (stride.Client, error) {
	clientID := cmd.Flag("clientid").Value.String()
	clientSecret := cmd.Flag("secret").Value.String()
	roomToken := cmd.Flag("token").Value.String()
	if (clientID == "" || clientSecret == "") && roomToken == "" {
		return nil, fmt.Errorf("you must provide a client id and secret, or a room token")
	}
	if clientID != "" && clientSecret != "" {
		return stride.NewClient(clientID, clientSecret, http.DefaultClient), nil
	}
	return stride.NewRoomClient(roomToken, http.DefaultClient), nil
}

func strideConfig(client stride.Client, cmd *cobra.Command) (cloudID, conversation string, err error) {
	cloudID = cmd.Flag("cloudid").Value.String()
	if cloudID == "" {
		err = fmt.Errorf("you must specify the Stride Cloud ID with STRIDE_CLOUD_ID or --cloudid")
		return "", "", err
	}
	if _, err = uuid.Parse(cloudID); err != nil {
		err = fmt.Errorf("invalid cloud id: %v", err)
		return "", "", err
	}
	conversation = cmd.Flag("conversation").Value.String()
	if conversation == "" {
		err = fmt.Errorf("you must specify the Stride conversation by ID or room name")
		return "", "", err
	}

	// Conversation may be a uuid or a name. If it's parseable as a uuid treat it as the conversation id.
	_, err = uuid.Parse(conversation)
	if err != nil {
		var conv *stride.ConversationCommon
		conv, err = client.GetConversationByName(cloudID, conversation)
		if err != nil {
			return "", "", err
		}
		if conv == nil {
			err = fmt.Errorf("Stride conversation not found")
			return "", "", err
		}
		conversation = conv.ID
	}
	return cloudID, conversation, err
}

func cmdMessage(cmd *cobra.Command, args []string) error {
	client, err := getClient(cmd)
	if err != nil {
		return err
	}
	cloudID, conversation, err := strideConfig(client, cmd)
	if err != nil {
		return err
	}

	// Make it easy to get a conversation ID, which means fewer round trips than sencind by conversation name.
	if cmd.Flag("lookup").Value.String() == "true" {
		fmt.Printf("Conversation ID is %s\n", conversation)
		return nil
	}

	message := strings.Join(args, " ")
	stride.SendText(client, cloudID, conversation, message)
	return nil
}

func cmdApplicationCard(cmd *cobra.Command, args []string) error {
	client, err := getClient(cmd)
	if err != nil {
		return err
	}
	cloudID, conversation, err := strideConfig(client, cmd)
	if err != nil {
		return err
	}

	text, title := args[0], args[1]
	description := cmd.Flag("description").Value.String()
	previewURL := cmd.Flag("previewurl").Value.String()
	if previewURL != "" {
		if !(strings.HasPrefix(previewURL, "https") || strings.HasPrefix(previewURL, "data:image/*")) {
			return fmt.Errorf("preview image URL must have a scheme of https:// or data:image/*")
		}
	}

	stride.SendCard(client, cloudID, conversation, text, title, description, previewURL)
	return nil
}
