package mock_stride

import (
	"bitbucket.org/atlassian/go-stride/pkg/stride"
	"fmt"
	"github.com/golang/mock/gomock"
	"testing"
)

type mockT struct {
	errorRecieved string
}

func (t *mockT) Errorf(format string, args ...interface{}) {
	t.errorRecieved = fmt.Sprintf(format, args...)
}

func (t *mockT) Fatalf(format string, args ...interface{}) {
	t.errorRecieved = fmt.Sprintf(format, args...)
}

func Test_mock_hasApiMethods(t *testing.T) {
	// mostly here to get the compiler to tell us what API interface bits are missing.
	ctrl := gomock.NewController(t)
	mock := NewMockClient(ctrl)

	mock.EXPECT().SendMessage("cloudID", "roomID", nil)
	mock.EXPECT().SendUserMessage("", "", nil)
	mock.EXPECT().GetConversation("", "")
	mock.EXPECT().GetConversationByName("", "")
	mock.EXPECT().GetUser("", "")
	mock.EXPECT().ConvertDocToText(nil)
	mock.EXPECT().ValidateToken("").Return(nil, nil)

	mock.SendMessage("cloudID", "roomID", nil)
	mock.SendUserMessage("", "", nil)
	mock.GetConversation("", "")
	mock.GetConversationByName("", "")
	mock.GetUser("", "")
	mock.ConvertDocToText(nil)
	mock.ValidateToken("")

	ctrl.Finish()
}

func Test_verify_passes(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mock := NewMockClient(ctrl)
	mock.EXPECT().SendMessage("cloudID", "roomID", nil)

	// present as a type assertion
	var client stride.Client = mock
	client.SendMessage("cloudID", "roomID", nil)
}

func Test_verify_fails(t *testing.T) {
	fakeT := &mockT{}
	ctrl := gomock.NewController(fakeT)

	mock := NewMockClient(ctrl)
	mock.EXPECT().SendMessage("cloudID", "roomID", nil)

	ctrl.Finish()
	if fakeT.errorRecieved !=
		"aborting test due to missing call(s)" {
		t.Errorf("Client mock did not finish with error for uncalled method SendMessage.")
	}
}
