package stride

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"encoding/json"
	"github.com/stretchr/testify/assert"
	"reflect"
)

func TestGetUser(t *testing.T) {
	testCases := []struct {
		name     string
		respData string
		err      error
	}{
		{
			name:     "valid response",
			respData: `{"schemas":["urn:ietf:params:scim:schemas:core:2.0:User"],"id":"1234:567890","userName":"aelse@atlassian.com","nickName":"alexander","active":true,"name":{"formatted":"Alexander Else","givenName":"Alexander","familyName":"Else"},"displayName":"Alexander Else","emails":[{"value":"aelse@atlassian.com","primary":true}],"meta":{"created":"2017-04-18T23:15:44.513Z","lastModified":"2017-09-22T01:09:10.401Z","resourceType":"User"},"title":"Engineering Principal","timezone":"Australia/Sydney","photos":[{"primary":true,"value":"https://avatar-cdn.atlassian.com/111111111111111111"}]}`,
			err:      nil,
		},
	}
	cloudID := "qwerty123"
	userID := "1234:567890"
	url := fmt.Sprintf("/scim/site/%s/Users/%s", cloudID, userID)
	for _, tc := range testCases {
		t.Logf("Running case %s", tc.name)
		mux := http.NewServeMux()
		server := httptest.NewServer(mux)

		mux.HandleFunc(url, func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, "%s", tc.respData)
		})
		mux.HandleFunc("/oauth/token", func(w http.ResponseWriter, r *http.Request) {
			if r.URL.String() != "/oauth/token" {
				t.Errorf("Incorrect URL = %v, want %v", r.URL, "/oauth/token")
			}
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, "%s", `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "expires_in": 3600,
                "scope": "participate:conversation manage:conversation",
                "token_type": "Bearer"
            }`)
		})

		client := &clientImpl{
			mutex:          &sync.Mutex{},
			apiBaseURL:     server.URL,
			authAPIBaseURL: server.URL,
			httpClient:     http.DefaultClient,
		}
		user, err := client.GetUser(cloudID, userID)
		server.Close()
		assert.Equal(t, tc.err, err)
		if user != nil {
			assert.Equal(t, userID, user.ID)
		}
	}
}

func TestGetConversationByName(t *testing.T) {
	_ = `{
	"cursor": "bogusbogusbogus",
	"size": 4,
	"limit": 75,
	"values": [
		{"name": "Horsefly", "type": "group", "id": "22345-6789-0123"},
		{"name": "Horse", "type": "group", "id": "12345-6789-0123"},
		{"name": "Goat", "type": "group", "id": "32345-6789-0123"},
		{"name": "Fly", "type": "group", "id": "42345-6789-0123"}
	]
}`
	testCases := []struct {
		name     string
		roomName string
		respData string
		expected *ConversationCommon
		err      error
	}{
		{
			name:     "Find room that is substring (at start) of other room",
			roomName: "Horse",
			respData: `{
			"cursor": "bogusbogusbogus",
			"size": 4,
			"limit": 75,
			"values": [
				{"name": "Horsefly", "type": "group", "id": "22345-6789-0123"},
				{"name": "Horse", "type": "group", "id": "12345-6789-0123"}
			]
			}`,
			expected: &ConversationCommon{Name: "Horse"},
			err:      nil,
		}, {
			name:     "Find room that is substring (not at start) of other room",
			roomName: "Fly",
			respData: `{
			"cursor": "bogusbogusbogus",
			"size": 2,
			"limit": 75,
			"values": [
				{"name": "Horsefly", "type": "group", "id": "22345-6789-0123"},
				{"name": "Fly", "type": "group", "id": "42345-6789-0123"}
			]
			}`,
			expected: &ConversationCommon{Name: "Fly"},
			err:      nil,
		}, {
			name:     "Do not find room that does not exist",
			roomName: "Zebra",
			respData: `{
			"cursor": "bogusbogusbogus",
			"size": 0,
			"limit": 75,
			"values": []
			}`,
			expected: nil,
			err:      nil,
		}, {
			name:     "Do not find room that is only substring of other room",
			roomName: "Go",
			respData: `{
			"cursor": "bogusbogusbogus",
			"size": 0,
			"limit": 75,
			"values": []
			}`,
			expected: nil,
			err:      nil,
		},
	}
	cloudID := "qwerty123"
	url := fmt.Sprintf("/site/%s/conversation", cloudID)
	for _, tc := range testCases {
		t.Logf("Running case %s", tc.name)
		mux := http.NewServeMux()
		server := httptest.NewServer(mux)

		mux.HandleFunc(url, func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, "%s", tc.respData)
		})
		mux.HandleFunc("/oauth/token", func(w http.ResponseWriter, r *http.Request) {
			if r.URL.String() != "/oauth/token" {
				t.Errorf("Incorrect URL = %v, want %v", r.URL, "/oauth/token")
			}
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, "%s", `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "expires_in": 3600,
                "scope": "participate:conversation manage:conversation",
                "token_type": "Bearer"
            }`)
		})

		client := &clientImpl{
			mutex:          &sync.Mutex{},
			apiBaseURL:     server.URL,
			authAPIBaseURL: server.URL,
			httpClient:     http.DefaultClient,
		}
		conv, err := client.GetConversationByName(cloudID, tc.roomName)
		server.Close()
		assert.Equal(t, tc.err, err)
		if conv == nil {
			// If we found nothing the test case should expect nothing.
			assert.Equal(t, tc.expected, conv)
		} else {
			t.Logf("Conversation %#v", conv)
			assert.Equal(t, tc.roomName, conv.Name)
		}
	}
}

func Test_ReadPayload(t *testing.T) {
	bs := []byte(`{"body":{"type":"doc","version":1,"content":[{"type":"paragraph","content":[{"type":"text","text":"The gorp builds are failing again #hashtag."}]}]}}`)
	actual, _ := ReadPayload(bs)

	expected := &Payload{
		Body: &Document{
			Version: 1,
			Content: []interface{}{
				&Paragraph{
					Content: []InlineGroupNode{
						&Text{
							Text: "The gorp builds are failing again #hashtag.",
						},
					},
				},
			},
		},
	}

	if !reflect.DeepEqual(expected, actual) {
		s, _ := json.Marshal(actual)
		t.Error(
			"Deserialized document did not match:",
			"\nExpected: ", string(bs),
			"\nActual:   ", string(s))
	}

}

func TestParagraphDocument(t *testing.T) {
	actual := ParagraphDocument("Testy McTestface.")

	expected := &Payload{
		Body: &Document{
			Version: 1,
			Content: []interface{}{
				&Paragraph{
					Content: []InlineGroupNode{
						&Text{
							Text: "Testy McTestface.",
						},
					},
				},
			},
		},
	}

	if !reflect.DeepEqual(expected, actual) {
		t.Error("Paragraph document did not match:",
			"\nExpected: ", expected,
			"\nActual:   ", actual)
	}
}

func TestApplicationCardDocument(t *testing.T) {
	testCases := []struct {
		name        string
		text        string
		title       string
		description string
		previewURL  string
		expected    *Payload
	}{
		{
			name:  "Basic application card",
			text:  "I am text.",
			title: "I am a title.",
			expected: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&ApplicationCard{
							&ApplicationCardAttrs{
								Text: "I am text.",
								Title: CardAttrTitle{
									Text: "I am a title.",
								},
							},
						},
					},
				},
			},
		},
		{
			name:        "Application card with description",
			text:        "I am text.",
			title:       "I am a title.",
			description: "I am a description.",
			expected: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&ApplicationCard{
							&ApplicationCardAttrs{
								Text: "I am text.",
								Title: CardAttrTitle{
									Text: "I am a title.",
								},
								Description: &CardAttrText{
									Text: "I am a description.",
								},
							},
						},
					},
				},
			},
		},
		{
			name:       "Application card with preview",
			text:       "I am text.",
			title:      "I am a title.",
			previewURL: "https://test.com/test.png",
			expected: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&ApplicationCard{
							&ApplicationCardAttrs{
								Text: "I am text.",
								Title: CardAttrTitle{
									Text: "I am a title.",
								},
								Preview: &URL{
									URL: "https://test.com/test.png",
								},
							},
						},
					},
				},
			},
		},
		{
			name:        "Application card with description and preview",
			text:        "I am text.",
			title:       "I am a title.",
			description: "I am a description.",
			previewURL:  "https://test.com/test.png",
			expected: &Payload{
				Body: &Document{
					Version: 1,
					Content: []interface{}{
						&ApplicationCard{
							&ApplicationCardAttrs{
								Text: "I am text.",
								Title: CardAttrTitle{
									Text: "I am a title.",
								},
								Description: &CardAttrText{
									Text: "I am a description.",
								},
								Preview: &URL{
									URL: "https://test.com/test.png",
								},
							},
						},
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Logf("Running case %s", tc.name)
		actual := ApplicationCardDocument(tc.text, tc.title, tc.description, tc.previewURL)

		if !reflect.DeepEqual(actual, tc.expected) {
			t.Error("ApplicationCard document did not match:",
				"\nExpected: ", tc.expected,
				"\nActual:   ", actual)
		}
	}
}
